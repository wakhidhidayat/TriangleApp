//
//  TriangleAppTests.swift
//  TriangleAppTests
//
//  Created by Wahid Hidayat on 29/11/21.
//

import XCTest
@testable import TriangleApp

class TriangleAppTests: XCTestCase {
    
    func testInvalidInputSides() {
        XCTAssertThrowsError(try detectTriangle(-1, 2, 3)) { error in
            XCTAssertEqual(error as? TriangleError, TriangleError.invalidInput)
        }
    }
    
    func detectTriangle(_ sideA: Int, _ sideB: Int, _ sideC: Int) throws -> TriangleResult {
        let sides = [sideA, sideB, sideC].sorted()
        for side in sides {
            if side < 1 {
                throw TriangleError.invalidInput
            }
        }
        
        if sides[0] + sides[1] <= sides[2] {
            throw TriangleError.inequalityInput
        } else if sides[0] == sides[1] && sides[0] == sides[2] {
            return TriangleResult.equateral
        } else if sides[0] == sides[1] || sides[1] == sides[2] {
            return TriangleResult.isosceles
        } else if Double(sides[0] * sides[0] + sides[1] * sides[1]).squareRoot() == Double(sides[2]) {
            return TriangleResult.pythagoras
        } else {
            return TriangleResult.random
        }
    }
    
    func testDetectEquateralTriangle() {
        XCTAssertEqual(try detectTriangle(10, 10, 10), TriangleResult.equateral)
    }
    
    func testDetectIsoscelesTriangle() {
        XCTAssertEqual(try detectTriangle(7, 10, 10), TriangleResult.isosceles)
        XCTAssertEqual(try detectTriangle(10, 7, 10), TriangleResult.isosceles)
        XCTAssertEqual(try detectTriangle(10, 10, 7), TriangleResult.isosceles)
    }
    
    func testDetectRandomTriangle() {
        XCTAssertEqual(try detectTriangle(7, 10, 5), TriangleResult.random)
    }
    
    func testInequalityTriangle() {
        XCTAssertThrowsError(try detectTriangle(4, 1, 2)) { error in
            XCTAssertEqual(error as? TriangleError, TriangleError.inequalityInput)
        }
        
        XCTAssertThrowsError(try detectTriangle(5, 1, 3)) { error in
            XCTAssertEqual(error as? TriangleError, TriangleError.inequalityInput)
        }
    }
    
    func testDetectPythagorasTriangle() {
        XCTAssertEqual(try detectTriangle(6, 8, 10), TriangleResult.pythagoras)
    }
    
    enum TriangleError: Error {
        
        case invalidInput
        case inequalityInput
        
    }
    
    enum TriangleResult: String {
        
        case equateral = "Segitiga sama sisi"
        case isosceles = "Segitiga sama kaki"
        case random = "Segitiga sembarang"
        case pythagoras = "Segitigas siku-siku"
        
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
